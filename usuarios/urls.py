from django.urls import path, include
from rest_framework import routers
from .views import UsuariosViewSet

router = routers.DefaultRouter()
router.register('usuarios', UsuariosViewSet, basename='usuarios')

urlpatterns = router.urls

